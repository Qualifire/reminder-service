package io.scrumbag.www.form;

import javax.validation.constraints.NotNull;

public class ReminderForm {
    @NotNull
    private String title;
    @NotNull
    private String content;
    @NotNull
    private long reminderDate;

     ReminderForm() {
    }

    public ReminderForm(String title, String content, long reminderDate) {
        this.title = title;
        this.content = content;
        this.reminderDate = reminderDate;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public long getReminderDate() {
        return reminderDate;
    }
}
