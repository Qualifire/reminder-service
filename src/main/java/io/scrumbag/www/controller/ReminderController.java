package io.scrumbag.www.controller;

import io.scrumbag.www.exceptions.ReminderNotFoundException;
import io.scrumbag.www.form.ReminderForm;
import io.scrumbag.www.model.Reminder;
import io.scrumbag.www.repo.ReminderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
public class ReminderController {
    final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    @Autowired
    private ReminderRepo reminderRepo;

    @RequestMapping(value = "/reminders", method = RequestMethod.GET)
    public List<Reminder> getAll() {
        return reminderRepo.findAll();
    }

    @RequestMapping(value = "/reminders/{id}", method = RequestMethod.GET)
    public Reminder getReminder(@PathVariable(value = "id") final String reminderId) {
        Reminder found = reminderRepo.findById(reminderId);
        if(found !=null){
            return found;
        }else{
            throw new ReminderNotFoundException();
        }
    }

    @RequestMapping(value = "/reminders/{id}", method = RequestMethod.POST)
    public ResponseEntity<?> updateReminder(@PathVariable(value = "id") final String reminderId,
                                            @RequestBody @Valid ReminderForm reminderForm) {
        Reminder r = new Reminder(reminderForm.getTitle(), reminderForm.getContent(), reminderForm.getReminderDate(), System.currentTimeMillis());
        r.setId(reminderId);
        reminderRepo.save(r);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/reminders/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteReminder(@PathVariable(value = "id") final String reminderId) {
        reminderRepo.delete(reminderId);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/reminders/due/{date}", method = RequestMethod.GET)
    public List<Reminder> getAllDueForDate(@PathVariable(value = "date") final String dateValue) {
        LocalDateTime ldt = LocalDateTime.of(LocalDate.parse(dateValue, DATE_FORMAT), LocalTime.MIDNIGHT);
        ZoneId zoneId = ZoneId.systemDefault();
        long epoch = ldt.atZone(zoneId).toEpochSecond() * 1000;
        return reminderRepo.findByReminderTimeGreaterThanEqual(epoch);
    }

    @RequestMapping(value = "/reminders", method = RequestMethod.POST)
    public ResponseEntity<?> createReminder(@RequestBody @Valid ReminderForm reminderForm) {
        Reminder r = new Reminder(reminderForm.getTitle(), reminderForm.getContent(), reminderForm.getReminderDate(), System.currentTimeMillis());
        reminderRepo.save(r);
        return ResponseEntity.ok().build();
    }


}
