package io.scrumbag.www.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Reminder {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private String id;
    private String title;
    private String content;
    private long reminderTime;
    private long creationTime;

    protected Reminder(){
    }


    public Reminder(String title, String content, long reminderTime, long creationTime) {
        this.title = title;
        this.content = content;
        this.reminderTime = reminderTime;
        this.creationTime = creationTime;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public String getId() {
        return id;
    }

    public long getReminderTime() {
        return reminderTime;
    }

    public String getTitle() {
        return title;
    }
}
