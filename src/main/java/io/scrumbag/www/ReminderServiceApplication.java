package io.scrumbag.www;

import io.scrumbag.www.model.Reminder;
import io.scrumbag.www.repo.ReminderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReminderServiceApplication implements CommandLineRunner{

	@Autowired
	private ReminderRepo reminderRepo;

	public static void main(String[] args) {
		SpringApplication.run(ReminderServiceApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		/*
		long currentMillis = System.currentTimeMillis();
		Instant instant = Instant.ofEpochMilli(currentMillis);
		LocalDateTime res = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
		*/

		reminderRepo.deleteAll();
		reminderRepo.save(new Reminder("test current", "blabla", System.currentTimeMillis(), System.currentTimeMillis()-(3600 * 1000 * 24)));
		reminderRepo.save(new Reminder("test old", "blabla", System.currentTimeMillis()-(3600 * 1000 * 48), System.currentTimeMillis()-(3600 * 1000 * 72)));


	}
}
