package io.scrumbag.www.repo;

import io.scrumbag.www.model.Reminder;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface ReminderRepo extends CrudRepository<Reminder, String> {

    @Override
    List<Reminder> findAll();

    //@Query("{ 'reminderTime' : {'$gte':?0} }")
    List<Reminder> findByReminderTimeGreaterThanEqual(long timeStamp);

    Reminder findById(String reminderId);
}
