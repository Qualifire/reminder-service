package io.scrumbag.www.exceptions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GenericExceptionHandler {

    @ExceptionHandler(ReminderNotFoundException.class)
    public ResponseEntity<?> handleArgumentException(final ReminderNotFoundException manve) {
        return ResponseEntity.badRequest().body("Not Found");
    }
}